#include <iostream>
using namespace std;

struct Car {
    int year;
    string make;
    string model;

    // default constructor
    Car () {
        cout << "default constructor" << endl;
        year = 2023;
        make = "Toyota";
        model = "Corolla";
    }

    // overloaded constructor
    // this-> is a pointer to your struct
    Car (int year, string make, string model) {
        cout << "overloaded constructor" << endl;
        this->year = year;
        this->make = make;
        this->model = model;
    }

    void start() {
        cout << "Vroom" << endl;
    }

    // // alternative to using this->
    // Car (int theYear, string theMake, string theModel) {
    //     cout << "overloaded constructor" << endl;
    //     year = theYear;
    //     make = theMake;
    //     model = theModel;
    // }
};


ostream& operator<<(ostream& os, const Car& car) {
    os << car.year << " " << car.make << " " << car.model;
    return os;
}


int main() {
    Car car1;
    Car car2(2021, "Tesla", "Model Y");

    // cout << car1.year << " " << car1.make << " " << car1.model << endl;
    // cout << car2.year << " " << car2.make << " " << car2.model << endl;
    
    cout << car1 << endl;
    cout << car2 << endl;

    car1.start();

    return 0;
}